"""Violin mode damping Guardian node.

When the node is ready, from the CL:

$ guardctrl create VIOLIN_DAMPER
$ guardmedm VIOLIN_DAMPER
$ guardctrl start VIOLIN_DAMPER
"""

from guardian import GuardState, GuardStateDecorator, NodeManager
from guardian.ligopath import userapps_path

import math
import numpy as numpy
import os
import subprocess as subp
import time
import cdsutils as cdsutils



def getpk(FM,dur):
    """read the FM output maximum value"""
    dmax=numpy.max(numpy.abs(cdsutils.getdata(FM+'_OUTPUT',dur).data))

def mylog(theMessage):
    notify(theMessage)
    log(theMessage)
    
def setHexaPhaseGain(FM,phase,gain):
    """set the FM combination"""
    if phase == 0:
        ezca.switch(FM, 'FM2', 'FM3', 'OFF')
        ezca[FM+'_GAIN'] = gain
    elif phase == 60:
        ezca.switch(FM, 'FM2', 'OFF', 'FM3', 'ON')
        ezca[FM+'_GAIN'] = gain
    elif phase == 300:
        ezca.switch(FM, 'FM2', 'ON', 'FM3', 'OFF')
        ezca[FM+'_GAIN'] = gain
    elif phase == 180:
        ezca.switch(FM, 'FM2', 'FM3', 'OFF')
        ezca[FM+'_GAIN'] = -gain
    elif phase == 240:
        ezca.switch(FM, 'FM2', 'OFF', 'FM3', 'ON')
        ezca[FM+'_GAIN'] = -gain
    elif phase == 120:
        ezca.switch(FM, 'FM2', 'ON', 'FM3', 'OFF')
        ezca[FM+'_GAIN'] = -gain
    else:
        ezca[FM+'_GAIN'] = 0
        
#######################################
# constant definitions

myFM='SUS-ETMY_L2_DAMP_MODE7'
myGain=20

########################################
# State Definitions

class INIT(GuardState):
    goto = True
    request = True
    def main(self):
        return True



# CHECKPOINT PLAYTHEVIOLIN
class VIOLIN_PHASE_FIND(GuardState):
    request = True
    goto = True

    def main(self):
        # initial setup
        self.myGain=myGain
        setHexaPhaseGain(myFM,0,self.myGain)
        self.dur = 5           # amount of data averaged (sec)
        self.measuretime = 120 # observe the damping for this amount of time (sec)
        self.waittime = 40     # wait this amount of time for filters to settle (sec)

        self.timer['pause'] = 0
        self.dmax = [0,0,0,0,0,0]
        self.dmax2 = [0,0,0,0,0,0]
        self.ratio = [0,0,0,0,0,0]
        self.growthAbort = 1.3
        self.abort = False
        self.counter = 0

    def run(self):
        ####################################################################################
        # p is state variable. For
        #   p<360: p=phase+q, where q:
        #     q=0: Once ready: write new phase filters and gain, then q=1, wait (waittime)
        #     q=1: Once ready: store beginning drive strength, then q=2, wait (measuretime)
        #     q=2: Calc. growith ratio. if > growthAbort, trigger timer.
        #          Once ready: store final drive strength, q=0, next phase, wait (0)
        #   p=360: Once ready: Cleanup: set mode to best damping, go to p=361, no wait
        #   p=361: no wait, do nothing, display result. State done.
        #####################################################################################
        # State order: 0,1,2,180,181,182,60,61,62,240,241,242,120,121,122,300,301,302,360,361
        #####################################################################################

        # get the state variables
        p=self.counter
        if p < 360:
            q=p%60
            phase=p-q
            n=phase/60
        else:
            q=-1
            phase=0
            n=0
        # do some logging
        mylog('Filter module: '+myFM+', Gain = '+str(self.myGain))
        mylog('Master State = '+str(p)+' ; Sub State = '+str(q)+' ; Phase = '+str(phase))
        mylog('Ratio for [0,60,120,180,240,300]: ['+str(self.ratio[0])+','+str(self.ratio[1])+','+str(self.ratio[2])+','+str(self.ratio[3])+','+str(self.ratio[4])+','+str(self.ratio[5])+']'  )
        # read the current strength; in state q=2: log it, and, if necessary, abort
        pmax=numpy.max(numpy.abs(cdsutils.getdata(myFM+'_OUTPUT',self.dur).data))
        if q == 2:
            mylog('Current strength: '+str(pmax)+', Beginning strength: '+str(self.dmax[n])+', Ratio: '+str(pmax/self.dmax[n]))
            if pmax/self.dmax[n] > self.growthAbort:
                # growing abort
                mylog('Mode is growing. Abort this damping setting now.')
                self.abort = True
        # Main state machine:
        if self.timer['pause']  and ( q == 0 ) :
            setHexaPhaseGain(myFM,phase,self.myGain)
            self.counter +=1
            self.timer['pause'] = self.waittime
        if self.timer['pause']  and ( q == 1 ) :
            self.dmax[n]=pmax
            self.counter +=1
            self.timer['pause'] = self.measuretime
        if (self.timer['pause'] or self.abort)  and ( q == 2 ) :
            self.abort = False
            self.dmax2[n]=pmax
            self.ratio[n]=self.dmax2[n]/self.dmax[n]
            #select next state: 0..180..60..240..120..300..360
            if phase==300:
                self.counter=360
            else:
                self.counter +=178 - 300*(phase/180)
            self.timer['pause'] = 0
        if self.timer['pause'] and self.counter == 360:
            # find min index
            nn=self.ratio.index(min(self.ratio))
            setHexaPhaseGain(myFM,nn*60,self.myGain)
            self.counter = 361
            mylog('Done with the state machine. Best setting: '+str(nn*60)+' deg, Ratio: '+str(self.ratio[nn]))
        if p==361:
            nn=self.ratio.index(min(self.ratio))
            mylog('Done with the state machine. Best setting: '+str(nn*60)+' deg, Ratio: '+str(self.ratio[nn]))
            return True


class SET_0(GuardState):
    index = 10
    request = True
    goto = True
    def main(self):
        setHexaPhaseGain(myFM,0,myGain)
        return True
    def run(self):
        return True

class SET_60(GuardState):
    index = 20
    request = True
    goto = True
    def main(self):
        setHexaPhaseGain(myFM,60,myGain)
        return True
    def run(self):
        return True

class SET_120(GuardState):
    index = 30
    request = True
    goto = True
    def main(self):
        setHexaPhaseGain(myFM,120,myGain)
        return True
    def run(self):
        return True

class SET_180(GuardState):
    index = 40
    request = True
    goto = True
    def main(self):
        setHexaPhaseGain(myFM,180,myGain)
        return True
    def run(self):
        return True

class SET_240(GuardState):
    index = 50
    request = True
    goto = True
    def main(self):
        setHexaPhaseGain(myFM,240,myGain)
        return True
    def run(self):
        return True

class SET_300(GuardState):
    index = 60
    request = True
    goto = True
    def main(self):
        setHexaPhaseGain(myFM,300,myGain)
        return True
    def run(self):
        return True



class DOWN(GuardState):
    request = True
    def main(self):
        return True
    def run(self):
        return True

class DAMPING(GuardState):
    request = True
    def main(self):
        return True
    def run(self):
        return True

########################################
# Edges

edges = [
    ('DAMPING', 'DOWN'),
    ('DOWN', 'DAMPING'),
    ]


